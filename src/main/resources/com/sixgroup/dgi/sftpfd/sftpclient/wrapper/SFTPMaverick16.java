package com.sixgroup.dgi.sftpfd.sftpclient.wrapper;

import java.io.FileNotFoundException;

import java.util.ArrayList;
import java.util.List;

import com.maverick.sftp.SftpFile;

import com.maverick.sftp.SftpStatusException;
import com.maverick.sftp.TransferCancelledException;
import com.maverick.ssh.PasswordAuthentication;
import com.maverick.ssh.SshClient;
import com.maverick.ssh.SshConnector;
import com.maverick.ssh.SshException;
import com.maverick.ssh2.Ssh2Client;
import com.maverick.ssh2.Ssh2Context;
import com.sshtools.net.SocketTransport;
import com.sshtools.publickey.ConsoleKnownHostsKeyVerification;
import com.sshtools.sftp.SftpClient;

public class SFTPMaverick16 extends SFTPAbstract {

	private SshConnector sshcon;
	private Ssh2Context ssh2Context;
	private SocketTransport socketTransport;
	private SshClient ssh;
	private Ssh2Client sshClient;
	private PasswordAuthentication pwd;
	private SftpClient sftp;

	public SFTPMaverick16() {
		super.getConfigParams();
	}


	public static void main (String[] args){
		SFTPMaverick16 mav = new SFTPMaverick16();
		mav.connect();
		List<String> files = mav.listDir();
		for (String file : files){
			System.out.println(file);
		}
		mav.disconnect();
	}
	
	@Override
	public void connect() {
		System.out.println("Connecting to " + sftpServer);
		try {
			/**
			 * Create an SshConnector instance
			 */
			sshcon = SshConnector.createInstance();

			// Lets do some host key verification
			ssh2Context = (Ssh2Context) sshcon.getContext(SshConnector.SSH2);
			ssh2Context.setHostKeyVerification(new ConsoleKnownHostsKeyVerification());
			ssh2Context.setPreferredPublicKey(Ssh2Context.PUBLIC_KEY_SSHDSS);

			/**
			 * Connect to the host
			 */
			socketTransport = new SocketTransport(sftpServer, port);
			socketTransport.setTcpNoDelay(true);

			ssh = sshcon.connect(socketTransport, username);

			sshClient = (Ssh2Client) ssh;
			/**
			 * Authenticate the user using password authentication
			 */
			pwd = new PasswordAuthentication();
			pwd.setPassword(password);

			/**
			 * Start a session and do basic IO
			 */
			if (ssh.isAuthenticated()) {

				System.out.println("Authenticated");
				sftp = new SftpClient(sshClient);

				/**
				 * Perform some text mode operations
				 */
				sftp.setTransferMode(SftpClient.MODE_TEXT);
			} else {
				System.out.println("Could not authenticate " + username);
				System.exit(1);
			}
		} catch (Throwable th) {
			System.out.println("Could not connect " + th);
			System.exit(1);
		}

	}

	@Override
	public void changeRemoteDir(String folder) {
		try {
			sftp.cd(folder);
		} catch (SftpStatusException | SshException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void downloadAllFiles(String localFolder, String remoteFolder) {

		try {
			sftp.getFiles(remoteFolder, localFolder);
		} catch (FileNotFoundException | TransferCancelledException | SftpStatusException | SshException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void makeRemoteDir(String folder) {
		try {
			sftp.mkdirs(folder);
		} catch (SftpStatusException | SshException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void uploadFiles(String localFolder, String remoteFolder) {
		try {
			sftp.put(localFolder, remoteFolder);
		} catch (FileNotFoundException | SftpStatusException | SshException | TransferCancelledException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void removeRemoteFiles(String folder) {
		try {
			sftp.rm(folder, true, true);
		} catch (SftpStatusException | SshException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public List<String> listDir() {
		/**
		 * get a file using getFiles with default no reg exp matching
		 */
		List<String> files = new ArrayList<>();
		SftpFile[] remotefiles = null;
		try {
			remotefiles = sftp.ls();
		} catch (SftpStatusException | SshException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (SftpFile file : remotefiles) {
			if ((!file.getFilename().equals(".")) | (!file.getFilename().equals(".."))) {
				files.add(file.getFilename());
			}
		}
		return files;

	}


	@Override
	public void setRemotePermission(int permissions, String path) {
		try {
			sftp.chmod(permissions, path);
		} catch (SftpStatusException | SshException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	@Override
	public void disconnect() {
		try {
			sftp.exit();
			sshClient.disconnect();
		} catch (SshException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}

}
