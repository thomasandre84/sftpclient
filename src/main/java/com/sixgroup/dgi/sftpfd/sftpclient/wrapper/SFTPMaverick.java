package com.sixgroup.dgi.sftpfd.sftpclient.wrapper;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import com.sshtools.net.SocketTransport;
import com.sshtools.publickey.ConsoleKnownHostsKeyVerification;
import com.sshtools.sftp.SftpClient;
import com.sshtools.sftp.SftpFile;
import com.sshtools.sftp.SftpStatusException;
import com.sshtools.sftp.TransferCancelledException;
import com.sshtools.ssh.PasswordAuthentication;
import com.sshtools.ssh.SshClient;
import com.sshtools.ssh.SshConnector;
import com.sshtools.ssh.SshException;
import com.sshtools.ssh2.Ssh2Context;

public class SFTPMaverick extends SFTPAbstract {

	private SshConnector sshcon;
	private Ssh2Context ssh2Context;
	private SocketTransport socketTransport;
	private SshClient sshClient;
	private PasswordAuthentication pwd;
	private SftpClient sftp;

	/**
	 * Contructor, requires the parameters, according to json.
	 * 
	 * @param map
	 *            a Map with the required information for the test, according to
	 *            json.
	 * @param threadnumber
	 *            the number of the thread
	 */
	public SFTPMaverick(Map<String, Object> map, int threadnumber) {
		username = (String) map.get("user");
		password = (String) map.get("password");
		sftpServer = (String) map.get("sftpfd");
		port = (int) map.get("port");
		remoteFolder = (String) map.get("remoted");
		localFolder = (String) map.get("locald");
		timeOut = (double) map.get("time_out");
		downloadFile = (String) map.get("download_file");
		this.threadnumber = threadnumber;

	}

	/**
	 * Contructor, requires the parameters, according to json.
	 * 
	 * @param map
	 *            a Map with the required information for the test, according to
	 *            json.
	 */
	public SFTPMaverick(Map<String, Object> map) {
		username = (String) map.get("user");
		password = (String) map.get("password");
		sftpServer = (String) map.get("sftpfd");
		port = (int) map.get("port");
		remoteFolder = (String) map.get("remoted");
		localFolder = (String) map.get("locald");
		timeOut = (double) map.get("time_out");
		downloadFile = (String) map.get("download_file");
		this.threadnumber = 1;

	}

	/**
	 * Contructor, requires the parameters, according to json.
	 * 
	 * @param JSONObject
	 *            a Map with the required information for the test, according to
	 *            json.
	 * @param threadnumber
	 *            the number of the thread
	 */
	public SFTPMaverick(JSONObject jsonObj, int threadnumber) {
		username = (String) jsonObj.get("user");
		password = (String) jsonObj.get("password");
		sftpServer = (String) jsonObj.get("sftpfd");
		port = (int) jsonObj.get("port");
		remoteFolder = (String) jsonObj.get("remoted");
		localFolder = (String) jsonObj.get("locald");
		timeOut = (double) jsonObj.get("time_out");
		downloadFile = (String) jsonObj.get("download_file");
		this.threadnumber = threadnumber;
	}

	@Override
	public void connect() {
		System.out.print("Tread " + threadnumber + ": ");
		System.out.println("Connecting to " + sftpServer);
		try {
			/**
			 * Create an SshConnector instance
			 */
			sshcon = SshConnector.createInstance();

			// Lets do some host key verification
			ssh2Context = (Ssh2Context) sshcon.getContext();
			ssh2Context.setHostKeyVerification(new ConsoleKnownHostsKeyVerification());
			ssh2Context.setPreferredPublicKey(Ssh2Context.PUBLIC_KEY_SSHDSS);

			/**
			 * Connect to the host
			 */
			socketTransport = new SocketTransport(sftpServer, port);
			socketTransport.setTcpNoDelay(true);

			sshClient = sshcon.connect(socketTransport, username);

			/**
			 * Authenticate the user using password authentication
			 */
			pwd = new PasswordAuthentication();
			pwd.setUsername(username);
			pwd.setPassword(password);

			int result = sshClient.authenticate(pwd);

			/**
			 * Start a session and do basic IO
			 */
			if (result == 1) {
				System.out.print("Tread " + threadnumber + ": ");
				System.out.println("Authenticated");
				sftp = new SftpClient(sshClient);

				/**
				 * Perform some Binary mode operations
				 */
				sftp.setTransferMode(SftpClient.MODE_BINARY);
			} else {
				System.out.print("Tread " + threadnumber + ": ");
				System.out.println("Could not authenticate " + username);
				System.exit(1);
			}
		} catch (Throwable th) {
			System.out.print("Tread " + threadnumber + ": ");
			System.out.println("Could not connect " + th);
			System.exit(1);
		}

	}

	@Override
	public void changeRemoteDir(String folder) {
		try {
			sftp.cd(folder);
			System.out.print("Tread " + threadnumber + ": ");
			System.out.println("Change directory to: " + folder);
		} catch (SftpStatusException | SshException e) {
			System.out.print("Tread " + threadnumber + ": ");
			System.out.println("Could not change directory: " + e.toString());
		}

	}

	@Override
	public void downloadAllFiles(String localFolder, String remoteFolder) {

		List<String> files = listFiles(remoteFolder);
		changeRemoteDir(remoteFolder);
		for (String file : files) {
			try {
				sftp.get(file, localFolder);
				System.out.print("Tread " + threadnumber + ": ");
				System.out.println("Download " + file);
			} catch (FileNotFoundException | TransferCancelledException | SftpStatusException | SshException e) {
				System.out.print("Tread " + threadnumber + ": ");
				System.out.println("Could not downlaod file: " + e.toString());
			}
		}

	}

	@Override
	public void makeRemoteDir(String folder) {
		try {
			sftp.mkdirs(folder);
			System.out.print("Tread " + threadnumber + ": ");
			System.out.println("Created Folder: " + folder);
		} catch (SftpStatusException | SshException e) {
			System.out.print("Tread " + threadnumber + ": ");
			System.out.println("Could not create remote directory: " + e.toString());
		}

	}

	@Override
	public void uploadFiles(String localFolder, String remoteFolder) {
		try {
			List<String> localfiles = listLocalFiles(localFolder);
			changeLocalDir(localFolder);
			changeRemoteDir(remoteFolder);
			for (String file : localfiles){
				sftp.put(file, file);
				System.out.print("Tread " + threadnumber + ": ");
				System.out.println("Uploading file: " + file + " from: "+ localFolder + " to: " + remoteFolder);
			}
			
		} catch (FileNotFoundException | SftpStatusException | SshException | TransferCancelledException e) {
			System.out.print("Tread " + threadnumber + ": ");
			System.out.println("Could not upload files: " + e.toString());
		}

	}

	@Override
	public void removeRemoteFiles(String folder) {
		try {
			sftp.rm(folder, true, true);
			System.out.print("Tread " + threadnumber + ": ");
			System.out.println("Removing remote files in: " + folder);
		} catch (SftpStatusException | SshException e) {
			System.out.print("Tread " + threadnumber + ": ");
			System.out.println("Could not remove remote files: " + e.toString());
		}
	}

	@Override
	public List<String> listDir() {
		List<String> files = new ArrayList<>();
		SftpFile[] remotefiles = null;
		try {
			remotefiles = sftp.ls();
			System.out.print("Tread " + threadnumber + ": ");
			System.out.println("Listdir in current remote folder");
		} catch (SftpStatusException | SshException e) {
			System.out.print("Tread " + threadnumber + ": ");
			System.out.println("Could not listdir in current folder " + e.toString());
		}
		for (SftpFile file : remotefiles) {
			files.add(file.getFilename());
		}
		return files;

	}

	@Override
	public List<String> listFiles(String remoteFolder) {
		List<String> files = new ArrayList<>();
		SftpFile[] remotefiles = null;
		try {
			remotefiles = sftp.ls();
			System.out.print("Tread " + threadnumber + ": ");
			System.out.println("Listing files in remote folder: " + remoteFolder);
		} catch (SftpStatusException | SshException e) {
			System.out.print("Tread " + threadnumber + ": ");
			System.out.println("Could not list remote folder: " + e.toString());
		}
		for (SftpFile file : remotefiles) {
			try {
				if ((!file.getFilename().equals(".")) | (!file.getFilename().equals("..")) | (!file.isDirectory())) {

					files.add(file.getFilename());
				}
			} catch (SftpStatusException | SshException e) {
				System.out.print("Tread " + threadnumber + ": ");
				System.out.println("Could not stat files in remoteFolder: " + e.toString());
			}
		}
		return files;
	}

	@Override
	public void setRemotePermission(int permissions, String path) {
		try {
			sftp.chmod(permissions, path);
			System.out.print("Tread " + threadnumber + ": ");
			System.out.println("Changed permissions to " + permissions + " in: " + path);
		} catch (SftpStatusException | SshException e) {
			System.out.print("Tread " + threadnumber + ": ");
			System.out.println("Could not change permissions: " + e.toString());
		}

	}

	@Override
	public void disconnect() {
		try {
			sftp.exit();
			sshClient.disconnect();
			System.out.println("Disconnected Thread: " + threadnumber);
		} catch (SshException e) {
			System.out.print("Tread " + threadnumber + ": ");
			System.out.println("Could not disconnect");
		}
	}

	@Override
	public void changeLocalDir(String folder) {
		try {
			sftp.lcd(folder);
			System.out.print("Tread " + threadnumber + ": ");
			System.out.println("Change localdir in sftp Session to: " + folder);
		} catch (SftpStatusException e) {
			System.out.print("Tread " + threadnumber + ": ");
			System.out.println("Could not lcd: " + e.toString());
		}

	}

}
