/**
 * 
 */
package com.sixgroup.dgi.sftpfd.sftpclient;

import java.util.ArrayList;
import java.util.List;


import org.json.JSONObject;

import com.sixgroup.dgi.sftpfd.sftpclient.wrapper.SFTPAbstract;
import com.sixgroup.dgi.sftpfd.sftpclient.wrapper.SFTPJsch;
import com.sixgroup.dgi.sftpfd.sftpclient.wrapper.SFTPMaverick;

/**
 * @author Thomas Andre
 *
 */
public class StartSftpTests {

	private static final String JSCH = "jsch";
	private static final String MAVERICK = "maverick";

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length != 1) {
			System.out.println("Usage: java <Programm> <sftpLib>");
			System.out.println("Please add a sftp as first argument, from [jsch|maverick]");
			System.exit(-1);
		}
		Integer threads = new Integer(1);
		JSONObject jsonObj = SFTPAbstract.getJSONConfig();

		if (jsonObj.has("client_threads")) {
			threads = (Integer) jsonObj.get("client_threads");
		}

		System.out.println("Starting with " + threads + " Threads");
		List<Thread> threadlist = new ArrayList<>();
		// For multithreading
		for (int i = 1; i <= threads; i++) {
			SFTPAbstract sftp = null;

			switch (args[0]) {
			case JSCH:
				sftp = new SFTPJsch(jsonObj, i);
				break;
			case MAVERICK:
				sftp = new SFTPMaverick(jsonObj, i);
				break;
			default:
				System.out.println("SFTP CLient " + args[0] + " is unknown. exiting");
				System.exit(-1);
			}

			Thread sftpthread = new Thread(sftp);
			sftpthread.start();
			threadlist.add(sftpthread);
		}

	}

}
