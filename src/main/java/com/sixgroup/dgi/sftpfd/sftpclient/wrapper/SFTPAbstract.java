/**
 * 
 */
package com.sixgroup.dgi.sftpfd.sftpclient.wrapper;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.json.JSONObject;

/**
 * @author Thomas Andre
 *
 */
public abstract class SFTPAbstract implements Runnable {

	protected String sftpServer;
	protected int port;
	protected String remoteFolder;
	protected String localFolder;
	protected String password;
	protected String username;
	protected double timeOut;
	protected String downloadFile;
	protected int threadnumber;

	/**
	 * Return JSON File return it as String.
	 * 
	 * @return String
	 */
	public static String readFile(String filename) {
		String result = "";
		try {
			BufferedReader br = new BufferedReader(new FileReader(filename));
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();
			while (line != null) {
				sb.append(line);
				line = br.readLine();
			}
			result = sb.toString();
			br.close();
		} catch (Exception e) {
			System.out.println("Could not read json-config: " + e);
		}
		return result;
	}

	/**
	 * Read Config Parameters from JSON and return them in a
	 * 
	 * @return Map
	 */
	public static Map<String, Object> getConfigFromJSON() {
		
		Map<String, Object> dataMap = new HashMap<>();
		JSONObject jobj = getJSONConfig();

		for (String key : jobj.keySet()) {
			dataMap.put(key, jobj.get(key));
		}

		return dataMap;
	}
	
	/**
	 * Read Config Parameters from JSON and return them in a
	 * 
	 * @return JSONObj
	 */
	public static JSONObject getJSONConfig() {
		String jsonData = readFile("sftpfd-test.json");
		JSONObject jobj = new JSONObject(jsonData);
		return jobj;
	}

	/**
	 * Method to get some Config Params from File.
	 */
	public void getConfigParams() {
		Properties prop = new Properties();
		InputStream input = null;

		try {
			input = new FileInputStream("./config.properties");
			prop.load(input);

			sftpServer = prop.getProperty("SFTPServer");
			port = Integer.parseInt(prop.getProperty("Port"));
			remoteFolder = prop.getProperty("RemoteDirectory");
			localFolder = prop.getProperty("LocalDirectory");
			password = prop.getProperty("Password");
			username = prop.getProperty("Username");

		} catch (IOException ex) {

			System.out.println("Could not read config File " + ex);
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					System.out.println("Could not close file " + e);
				}
			}
		}

	}

	/**
	 * Build up a Connection.
	 */
	public abstract void connect();

	/**
	 * Chnage remote to the specified folder.
	 * 
	 * @param folder
	 */
	public abstract void changeRemoteDir(String folder);

	/**
	 * Show all files in the current folder.
	 * 
	 * @return List of files.
	 */
	public abstract List<String> listDir();

	/**
	 * Download all files from remoteFolder to localFolder.
	 * 
	 * @param localFolder
	 * @param remoteFolder
	 */
	public abstract void downloadAllFiles(String remoteFolder, String localFolder);

	/**
	 * Create a local directory for Downloads and uploads.
	 * 
	 * @param folder
	 *            a subfoldername
	 */
	public void makeLocalDir(String folder) {
		Path dir = FileSystems.getDefault().getPath(folder);
		try {
			Files.createDirectory(dir);
		} catch (IOException e) {
			System.out.print("Tread " + threadnumber + ": ");
			System.out.println("Could not create Directory " + dir.toString());
		}

	}

	/**
	 * Create a directory on the sftp-Server.
	 * 
	 * @param folder
	 *            the remote directory
	 */
	public abstract void makeRemoteDir(String folder);

	/**
	 * Upload all files from the localFolder to the remoteFolder.
	 * 
	 * @param localFolder
	 * @param remoteFolder
	 */
	public abstract void uploadFiles(String localFolder, String remoteFolder);

	/**
	 * Removes local Files recursive, downloaded by a Thread:
	 * 
	 * @param folder
	 *            an existing directory
	 */
	public void removeLocalFiles(String folder) {
		Path path = FileSystems.getDefault().getPath(folder);
		System.out.print("Tread " + threadnumber + ": ");
		System.out.println("Removing " + folder);
		try {
			Files.walk(path).sorted((a, b) -> b.compareTo(a)).forEach((p -> {
				try {
					Files.delete(p);
				} catch (IOException e) {
					System.out.println("Could not delete " + p.toString());
				}
			}));
		} catch (IOException e) {
			System.out.print("Tread " + threadnumber + ": ");
			System.out.println("Could not access " + path.toString());
		}

	}

	/**
	 * Returns a list of Files.
	 * 
	 * @param folder
	 *            the localFolder
	 * @return List of Files
	 */
	public List<String> listLocalFiles(String folder) {
		List<String> files = new ArrayList<>();
		Path path = FileSystems.getDefault().getPath(folder);
		System.out.print("Tread " + threadnumber + ": ");
		System.out.println("List localfiles");
		try{
			Files.walk(path).forEach(p -> {
				if (!p.toString().equals(folder)){
					String[] pathes = p.toString().split("/");
					if (pathes.length > 0)
						files.add(pathes[pathes.length -1]); 
				}
					
				});
		}catch (IOException e) {
			System.out.println("Could list Files in Folder " + folder + e.toString());
		}
		System.out.print("Thread " + threadnumber + " local Files: ");
		files.forEach(p -> System.out.print(p + " "));
		System.out.println();
		return files;
	}

	/**
	 * Remove the remote created Files incl. Folder.
	 * 
	 * @param folder
	 *            the remote Folder to delete.
	 */
	public abstract void removeRemoteFiles(String folder);

	/**
	 * Set filePermissions on a remote File or Folder.
	 * 
	 * @param permissions
	 * @param path
	 */
	public abstract void setRemotePermission(int permissions, String path);

	/**
	 * Disconnect from the server.
	 */
	public abstract void disconnect();

	/**
	 * List Files only, of s specific remote directory
	 * 
	 * @return List<String>
	 */
	public abstract List<String> listFiles(String remoteFolder);

	/**
	 * Change locally to the folder.
	 * 
	 * @param folder
	 */
	public abstract void changeLocalDir(String folder);
	
	@Override
	public void run() {
		connect();
		changeRemoteDir(remoteFolder);
		List<String> files = listDir();
		System.out.print("Tread " + threadnumber + ": ");
		System.out.print("RemoteFiles: ");
		files.forEach(v -> System.out.print(v + " "));
		System.out.println();
		String localThreadFolder = localFolder + "/" + threadnumber;
		makeLocalDir(localThreadFolder);
		changeLocalDir(localThreadFolder);
		downloadAllFiles(localThreadFolder, remoteFolder);
		String remoteThreadFolder = remoteFolder + "/" + threadnumber;
		makeRemoteDir(remoteThreadFolder);
		uploadFiles(localThreadFolder, remoteThreadFolder);
		setRemotePermission(0777, remoteThreadFolder);
		removeRemoteFiles(remoteThreadFolder);
		removeLocalFiles(localThreadFolder);
		disconnect();

	}

}
