/**
 * 
 */
package com.sixgroup.dgi.sftpfd.sftpclient.wrapper;

import java.util.ArrayList;

import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.json.JSONObject;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

/**
 * @author Thomas Andre
 * 
 *
 */
public class SFTPJsch extends SFTPAbstract {

	private Session session;
	private Channel channel;
	private ChannelSftp channelSftp;
	private JSch jsch;

	/**
	 * Contructor, requires the parameters, according to json.
	 * 
	 * @param map
	 *            a Map with the required information for the test, according to
	 *            json.
	 * @param threadnumber
	 *            the number of the thread
	 */
	public SFTPJsch(Map<String, Object> map, int threadnumber) {
		username = (String) map.get("user");
		password = (String) map.get("password");
		sftpServer = (String) map.get("sftpfd");
		port = (int) map.get("port");
		remoteFolder = (String) map.get("remoted");
		localFolder = (String) map.get("locald");
		timeOut = (double) map.get("time_out");
		downloadFile = (String) map.get("download_file");
		this.threadnumber = threadnumber;

	}

	/**
	 * Contructor, requires the parameters, according to json.
	 * 
	 * @param map
	 *            a Map with the required information for the test, according to
	 *            json.
	 */
	public SFTPJsch(Map<String, Object> map) {
		username = (String) map.get("user");
		password = (String) map.get("password");
		sftpServer = (String) map.get("sftpfd");
		port = (int) map.get("port");
		remoteFolder = (String) map.get("remoted");
		localFolder = (String) map.get("locald");
		timeOut = (double) map.get("time_out");
		downloadFile = (String) map.get("download_file");
		this.threadnumber = 1;

	}
	

	/**
	 * Contructor, requires the parameters, according to json.
	 * 
	 * @param JSONObject
	 *            a Map with the required information for the test, according to
	 *            json.
	 * @param threadnumber
	 *            the number of the thread
	 */
	public SFTPJsch(JSONObject jsonObj, int threadnumber) {
		username = (String) jsonObj.get("user");
		password = (String) jsonObj.get("password");
		sftpServer = (String) jsonObj.get("sftpfd");
		port = (int) jsonObj.get("port");
		remoteFolder = (String) jsonObj.get("remoted");
		localFolder = (String) jsonObj.get("locald");
		timeOut = (double) jsonObj.get("time_out");
		downloadFile = (String) jsonObj.get("download_file");
		this.threadnumber = threadnumber;
	}

	@Override
	public void connect() {
		jsch = new JSch();
		System.out.print("Tread " + threadnumber + ": ");
		System.out.println("Connecting to " + sftpServer);
		try {
			session = jsch.getSession(username, sftpServer, port);
			session.setPassword(password);
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.connect();
			System.out.println("Host connected.");
			channel = session.openChannel("sftp");
			channel.connect();
			channelSftp = (ChannelSftp) channel;
			System.out.print("Tread " + threadnumber + ": ");
			System.out.println("sftp channel opened and connected.");

		} catch (JSchException e) {
			System.out.print("Tread " + threadnumber + ": ");
			System.out.println("Could not connect: " + e.toString());
		}

	}

	@Override
	public void changeRemoteDir(String folder) {
		try {
			channelSftp.cd(folder);
			System.out.print("Tread " + threadnumber + ": ");
			System.out.println("Changed remote dir to: " + folder);
		} catch (SftpException e) {
			System.out.print("Tread " + threadnumber + ": ");
			System.out.println("Could not changeDir " + e.toString());
		}

	}

	@Override
	public List<String> listDir() {
		List<String> dir = new ArrayList<>();

		try {
			String curdir = channelSftp.pwd();
			@SuppressWarnings("unchecked")
			Vector<LsEntry> dirv = channelSftp.ls(curdir);
			dirv.forEach(p -> dir.add(p.getFilename()));
			System.out.print("Tread " + threadnumber + ": ");
			System.out.println("Listing current directory " + curdir);
		} catch (SftpException e) {
			System.out.print("Tread " + threadnumber + ": ");
			System.out.println("Could not list remote Folder: " + e.toString());
		}
		return dir;
	}

	@Override
	public void downloadAllFiles(String remoteFolder, String localFolder) {
		List<String> files = listFiles(remoteFolder);
		files.forEach(p -> {
			try {
				channelSftp.get(p, p);
				System.out.print("Tread " + threadnumber + ": ");
				System.out.println("Downloaded: " + p);
			} catch (SftpException e) {
				System.out.print("Tread " + threadnumber + ": ");
				System.out.println("Could not download Files: " + e.toString());
			}
		});

	}

	@Override
	public void makeRemoteDir(String folder) {
		try {
			channelSftp.mkdir(folder);
			System.out.println("Made remote directory: " + folder);
		} catch (SftpException e) {
			System.out.print("Tread " + threadnumber + ": ");
			System.out.println("Could not make RemoteDir: " + e.toString());
		}
	}

	@Override
	public void uploadFiles(String localFolder, String remoteFolder) {
		List<String> files = listLocalFiles(localFolder);
		changeLocalDir(localFolder);
		changeRemoteDir(remoteFolder);
		files.forEach(p -> {
			try {
				channelSftp.put(p, p);
				System.out.println("Uploading: " + p);
			} catch (SftpException e) {
				System.out.print("Tread " + threadnumber + ": ");
				System.out.println("Could not upload Files: " + e.toString());
			}
		});
	}

	@Override
	public void removeRemoteFiles(String folder) {
		try {
			channelSftp.rm(folder);
		} catch (SftpException e) {
			System.out.print("Tread " + threadnumber + ": ");
			System.out.println("Could not remove RemoteFiles: " + e.toString());
		}

	}

	@Override
	public void setRemotePermission(int permissions, String path) {
		try {
			channelSftp.chmod(permissions, path);
		} catch (SftpException e) {
			System.out.print("Tread " + threadnumber + ": ");
			System.out.println("Could not Change RemotePermissions: " + e.toString());
		}

	}

	@Override
	public void disconnect() {

		channelSftp.exit();
		System.out.println("sftp Channel exited.");
		channel.disconnect();
		System.out.println("Channel disconnected.");
		session.disconnect();
		System.out.println("Host Session disconnected.");

	}

	@Override
	public List<String> listFiles(String remoteFolder) {
		List<String> files = new ArrayList<>();
		changeRemoteDir(remoteFolder);
		files = listDir();
		return files;
	}

	@Override
	public void changeLocalDir(String folder) {
		try {
			channelSftp.lcd(folder);
		} catch (SftpException e) {
			System.out.print("Tread " + threadnumber + ": ");
			System.out.println("Could not local Directory: " + e.toString());
		}

	}

}
